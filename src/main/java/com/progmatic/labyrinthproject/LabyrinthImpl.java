package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pappgergely
 */
public class LabyrinthImpl implements Labyrinth {

    private int width;
    private int height;
    CellType[][] labyrinthCell;
    Coordinate playerPosition;

    public LabyrinthImpl() {

        width = -1;
        height = -1;
        playerPosition = new Coordinate(width, height);

    }

    @Override
    public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));

            width = Integer.parseInt(sc.nextLine());
            height = Integer.parseInt(sc.nextLine());

            labyrinthCell = new CellType[width][height];

            CellType cell;

            for (int hh = 0; hh < height; hh++) {
                String line = sc.nextLine();
                for (int ww = 0; ww < width; ww++) {
                    switch (line.charAt(ww)) {
                        case 'W':
                            cell = CellType.WALL;
                            break;
                        case 'E':
                            cell = CellType.END;
                            break;
                        case 'S':
                            cell = CellType.START;
                            playerPosition = new Coordinate(ww, hh);
                            break;
                        default:
                            cell = CellType.EMPTY;
                            break;
                    }
                    Coordinate cord = new Coordinate(ww, hh);
                    setCellType(cord, cell);
                }
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());

        } catch (CellException ex) {
            Logger.getLogger(LabyrinthImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public CellType getCellType(Coordinate c) throws CellException {
        if (c.getRow() >= height || c.getCol() >= width) {
            throw new CellException(height, width, "Problem with the labyrinth cell with coordinates(" + c.getRow() + ", " + c.getCol() + ").");
        }
        if (c.getRow() < 0 || c.getCol() < 0) {
            throw new CellException(height, width, "Problem with the labyrinth cell with coordinates(" + c.getRow() + ", " + c.getCol() + ").");
        }
        return labyrinthCell[c.getCol()][c.getRow()];
    }

    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
        labyrinthCell = new CellType[width][height];
    }

    @Override
    public void setCellType(Coordinate c, CellType type) throws CellException {
        if (c.getRow() >= height || c.getCol() >= width) {
            throw new CellException(height, width, "Problem with the labyrinth cell with coordinates(" + c.getRow() + ", " + c.getCol() + ").");
        }
        if (c.getRow() < 0 || c.getCol() < 0) {
            throw new CellException(height, width, "Problem with the labyrinth cell with coordinates(" + c.getRow() + ", " + c.getCol() + ").");
        }

        labyrinthCell[c.getCol()][c.getRow()] = type;
        if (type.equals(CellType.START)) {
            playerPosition = c;
        }
    }

    @Override
    public Coordinate getPlayerPosition() {
        return playerPosition;
    }

    @Override
    public boolean hasPlayerFinished() {

        int col = getPlayerPosition().getCol();
        int row = getPlayerPosition().getRow();

        if (labyrinthCell[col][row].equals(CellType.END)) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public List<Direction> possibleMoves() {
        List<Direction> directionList = new ArrayList<>();
        int col = getPlayerPosition().getCol();
        int row = getPlayerPosition().getRow();

        if (playerPosition.getRow() + 1 < labyrinthCell.length
                && !labyrinthCell[col][row + 1].equals(CellType.WALL)) {
            directionList.add(Direction.SOUTH);
        } else if (playerPosition.getRow() - 1 > 0
                && !labyrinthCell[col][row - 1].equals(CellType.WALL)) {
            directionList.add(Direction.NORTH);
        } else if (playerPosition.getCol() + 1 > 0
                && !labyrinthCell[col + 1][row].equals(CellType.WALL)) {
            directionList.add(Direction.WEST);
        } else if (playerPosition.getCol() - 1 < labyrinthCell.length
                && !labyrinthCell[col - 1][row].equals(CellType.WALL)) {
            directionList.add(Direction.EAST);
        }
        return directionList;
    }

    @Override
    public void movePlayer(Direction direction) throws InvalidMoveException {
        if (!possibleMoves().contains(direction)) {
            throw new InvalidMoveException();
        }
        if (direction.equals(Direction.SOUTH)) {
            playerPosition = new Coordinate(playerPosition.getCol(), playerPosition.getRow() + 1);
        }
        if (direction.equals(Direction.NORTH)) {
            playerPosition = new Coordinate(playerPosition.getCol(), playerPosition.getRow() - 1);
        }
        if (direction.equals(Direction.WEST)) {
            playerPosition = new Coordinate(playerPosition.getCol() + 1, playerPosition.getRow());
        }
        if (direction.equals(Direction.EAST)) {
            playerPosition = new Coordinate(playerPosition.getCol() - 1, playerPosition.getRow());
        }
    }

}
